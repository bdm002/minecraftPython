// https://projects.raspberrypi.org/en/projects/getting-started-with-minecraft-pi/1
// $ sudo apt-get  update && sudo  apt-get  install  minecraft-pi
// <Tab> key 로서 이동한다. 잊지말도록.   한쪽은  Thonny Editer 로서 편집하면서.
// [key]   w a s d e  space,  double_space, esc,  tab   <E> 자원 
// [Block Name]   https://minecraft-ids.grahamedgecombe.com/
// [원점]  (0, 10, 0)    [전망 좋은 곳]  (-10, 10 -10)
// [scratch]  $ curl http://scratch2mcpi.github.io/install.sh | sh

# 01 Basic  (위치, 이동, 블록 )

from mcpi import minecraft
mc = minecraft.Minecraft.create()

x, y, z = mc.player.getPos()

# mc.postToChat("Hello world")
# mc.player.setPos(x, y+100, z)

# mc.setBlock(x+1, y, z, 2)
# mc.setBlocks(x+1, y+1, z+1, x+11, y+11, z+11, stone)

mc.setBlock(x, y, z, 46)    // 46=TNT
mc.setBlocks(x+1, y+1, z+1, x+11, y+11, z+11, 46, 1)



#02  flowing lava

from mcpi.minecraft import Minecraft
mc = Minecraft.create()

x, y, z = mc.player.getPos()
lava = 10
mc.setBlock(x+3, y+3, z, lava)


#03 flow

from mcpi.minecraft import Minecraft
from time import sleep

mc = Minecraft.create()

x, y, z = mc.player.getPos()

lava = 10
water = 8
air = 0

mc.setBlock(x+3, y+3, z, lava)
sleep(20)
mc.setBlock(x+3, y+5, z, water)
sleep(4)
mc.setBlock(x+3, y+5, z, air)