# 01 stairs
from mcpi.minecraft import Minecraft
mc = Minecraft.create()

pos = mc.player.getTilePos()
x, y, z = pos.x, pos.y, pos.z

stairBlock = 53

for step in range(10):
    mc.setBlock(x + step, y + step, z, stairBlock)
    
    


# 02  pillars.py

from mcpi.minecraft import Minecraft
mc = Minecraft.create()


def setPillar(x, y, z, height):
    """Creates a pillar. Args set position and height of pillar"""
    stairBlock = 156
    block = 155

    # Pillar top
    mc.setBlocks(x - 1, y + height, z - 1, x + 1, y + height, z + 1, block, 1)
    mc.setBlock(x - 1, y + height - 1, z, stairBlock, 12)
    mc.setBlock(x + 1, y + height - 1, z, stairBlock, 13)
    mc.setBlock(x, y + height - 1, z + 1, stairBlock, 15)
    mc.setBlock(x, y + height - 1, z - 1, stairBlock, 14)

    # Pillar base
    mc.setBlocks(x - 1, y, z - 1, x + 1, y, z + 1, block, 1)
    mc.setBlock(x - 1, y + 1, z, stairBlock, 0)
    mc.setBlock(x + 1, y + 1, z, stairBlock, 1)
    mc.setBlock(x, y + 1, z + 1, stairBlock, 3)
    mc.setBlock(x, y + 1, z - 1, stairBlock, 2)

    # Pillar column
    mc.setBlocks(x, y, z, x, y + height, z, block, 2)

pos = mc.player.getTilePos()
x, y, z = pos.x + 2, pos.y, pos.z

for xOffset in range(0, 100, 5):
    setPillar(x + xOffset, y, z, 10)
    
    

# 02 pyramid.py

from mcpi.minecraft import Minecraft
mc = Minecraft.create()


block = 24  # sandstone
height = 10
levels = reversed(range(height))

pos = mc.player.getTilePos()
x, y, z = pos.x + height, pos.y, pos.z

for level in levels:
    mc.setBlocks(x - level, y, z - level, x + level, y, z + level, block)
    y += 1
    
    
    
# 03 rainbow stack.py

from mcpi.minecraft import Minecraft
mc = Minecraft.create()

oneDimensionalRainbowList = [0, 1, 2, 3, 4, 5]
pos = mc.player.getTilePos()
x = pos.x
y = pos.y
z = pos.z

for color in oneDimensionalRainbowList:
    mc.setBlock(x, y, z, 35, color)
    y += 1
    
    